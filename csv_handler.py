import csv


class csv_writer(object):
    """
    Class which handles writing to CSV
    """
    def __init__(self):
        super().__init__()

    def write_to_file(self, shapes, path):
        extension = path.split('.')
        if len(extension) != 2 or extension[1] != '.csv':
            pad = extension[0] + '.csv'

        with open(pad, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['Name', 'Width', 'Height'])
            for shape in shapes:
                writer.writerow(shape)

        if __debug__:
            print("Written file to {}".format(path))
            print(shapes)


class csv_reader(object):
    """
    Class which handles reading from CSV.
    """
    def __init__(self):
        self.shapes = []
        super().__init__()

    def read_from_file(self, path):
        self.path = path
        with open(path, newline='\n') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in reader:
                try:
                    name = row[0]
                    width = int(row[1])
                    height = int(row[2])
                    self.shapes.append([name, width, height])
                except ValueError:
                    continue

        if __debug__:
            print("Done reading CSV")
            print(self.shapes)
        return self.shapes


class csv_handler(csv_reader, csv_writer):
    def __init__(self):
        csv_reader.__init__(self)
        csv_writer.__init__(self)
