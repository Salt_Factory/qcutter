import sys
import csv_handler
import Cut_Planner
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QAction,
                             QTabWidget, QVBoxLayout, QPushButton,
                             QInputDialog, QLineEdit, QLabel, QTableWidget,
                             QGridLayout, QTableWidgetItem,
                             QAbstractScrollArea, QComboBox, QHBoxLayout,
                             QCompleter, QSizePolicy, QFrame, QCheckBox,
                             QRadioButton, QSpacerItem)
from PyQt5.QtGui import (QIcon, QIntValidator, QFont, QColor, QPixmap, QPen,
                         QBrush, QPainter, QPalette)
from PyQt5.QtCore import pyqtSlot, Qt


class controller():
    def __init__(self):
        self.csv = csv_handler.csv_handler()
        self.solution = None
        self.shapes = None
        app = QApplication(sys.argv)
        self.ex = main_window(self)
        sys.exit(app.exec_())

    def new_shape_row(self, row, column):
        shape_widget = self.ex.main_widget.setting_widget.shape_widget
        table = shape_widget.table
        if row == table.rowCount() - 2 and column == table.columnCount() - 1:
            shape_widget.new_row()
        shape_widget.update_table()

    def plan(self):
        setting_widget = self.ex.main_widget.setting_widget
        tab_solutions_widget = self.ex.main_widget.tab_solutions_widget
        table = setting_widget.shape_widget.table

        # First we check if the board parameters are correct.
        if not setting_widget.check_input():
            return
        b_width, b_height = setting_widget.get_input()
        with self.init_cut_planner() as cp:
            # Now we enter each valid input into the cp
            cp.clear()
            cp.add_board(b_width, b_height)

            # Clear the shapes list, create the shapes and append them.
            self.shapes = {}
            for row in range(table.rowCount()):
                try:
                    name = table.item(row, 0).text()
                    width = int(table.item(row, 1).text())
                    height = int(table.item(row, 2).text())

                    # Check if the values are correct
                    if width <= 0 or height <= 0:
                        raise ValueError("Positive width and height needed.")
                    if (width > max(b_width, b_height) or
                            height > max(b_width, b_height)):
                        raise ValueError("Shape too long/wide.")
                    if (width > min(b_width, b_height) and
                            height > min(b_width, b_height)):
                        raise ValueError("Shape too long/wide.")
                    print(name, width, height)
                    self.shapes[name] = cp.add_shape(width, height)
                    table.deactivate_wrong_row(row)  # Deactivate alarm.
                except (AttributeError, ValueError):
                    print(row)
                    # input()
                    # If an error occurs, activate the alarm.
                    table.activate_wrong_row(row)
            self.solution = cp.plan_cuts()
            tab_solutions_widget.show_solution(self.solution, self.shapes)

    def init_cut_planner(self):
        cp = Cut_Planner.Cut_Planner()
        return cp

    def remove_row(self):
        # Fetch the table
        table = self.ex.main_widget.setting_widget.shape_widget.table
        for row in range(table.rowCount()):
            for col in range(table.columnCount()):
                try:
                    if table.item(row, col).isSelected():
                        table.removeRow(row)
                except AttributeError:
                    continue
        print("A")

    def save_shapes(self):
        table = self.ex.main_widget.setting_widget.shape_widget.table
        shapes = []
        for row in range(table.rowCount()):
            try:
                name = table.item(row, 0).text()
                width = table.item(row, 1).text()
                height = table.item(row, 2).text()
                shapes.append([name, width, height])
            except AttributeError:
                continue

        self.csv.write_to_file(shapes, "shapes.csv")

    def load_shapes(self):
        table = self.ex.main_widget.setting_widget.shape_widget.table
        shapes = self.csv.read_from_file("shapes.csv")

        # Firstly prepare the table, add or remove rows if necessary
        table.setRowCount(len(shapes))

        for row, shape in enumerate(shapes):
            name = QTableWidgetItem(shape[0])
            width = QTableWidgetItem(str(shape[1]))
            height = QTableWidgetItem(str(shape[2]))

            table.setItem(row, 0, name)
            table.setItem(row, 1, width)
            table.setItem(row, 2, height)

    def save_to_png(self):
        from PIL import Image, ImageDraw

        # self.shapes holds the index for each shapename,
        # we need the inverse of this to map indices on names.
        shapenames = {x: y for y, x in self.shapes.items()}
        for board_index, shapes in zip(self.solution.keys(),
                                       self.solution.values()):
            board = shapes[0]
            img = Image.new("1", (board[0], board[1]), color=1)
            draw = ImageDraw.Draw(img)
            for shape in shapes[1:]:
                shape_index = shape[0]
                tl = shape[1]
                br = shape[2]

                width = br[0] - tl[0]
                height = br[1] - tl[1]
                name = shapenames[shape_index]

                # Needed to nicely center the text.
                w1, h1 = draw.textsize(str(width))
                w2, h2 = draw.textsize(str(height))
                w3, h3 = draw.textsize(str(name))

                text_pos1 = (round((width - w1)/2) + tl[0], tl[1])
                text_pos2 = (tl[0], round((height - h2)/2) + tl[1])
                text_pos3 = (round((width - w3)/2) + tl[0],
                             round((height/2 - h3)/2) + tl[1])

                draw.rectangle([tl, br], 1, 0)
                draw.text(text_pos1, str(width))
                draw.text(text_pos2, str(height))
                draw.text(text_pos3, shapenames[shape_index])
            del draw
            img.save("board{}.png".format(board_index), "PNG")


class main_window(QMainWindow):
    """
    The main window, containing the entire GUI.
    """
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.title = 'Qt Cut Planner'
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.left = 0
        self.top = 0
        self.width = 1200
        self.height = 700
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.main_widget = main_widget(self.c)
        self.setCentralWidget(self.main_widget)
        # self.tab_window = tab_window(self.c)
        # self.setCentralWidget(self.tab_window)
        self.show()


class main_widget(QWidget):
    """
    The main widget, containing all the others.
    """
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QHBoxLayout()
        self.initUI()

    def initUI(self):
        self.setting_widget = setting_widget(self.c)
        self.tab_solutions_widget = tab_solutions_widget(self.c)

        self.layout.addWidget(self.setting_widget)
        self.layout.addWidget(seperator("V"))
        self.layout.addWidget(self.tab_solutions_widget)
        self.setLayout(self.layout)


class setting_widget(QWidget):
    """
    The window containing the settings, some buttons and a table to enter the
    shape data.
    """
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.board_title = qustom_title("Board:")
        self.shapes_title = qustom_title("Shapes:")
        self.init_buttons()
        self.init_board_settings()

        self.shape_widget = shape_widget(self.c)

        self.layout.addWidget(self.board_title)
        self.layout.addWidget(self.board_widget)
        self.layout.addWidget(self.button_widget)
        self.layout.addWidget(seperator("H"))
        self.layout.addWidget(self.shapes_title)
        self.layout.addWidget(self.shape_widget)
        self.setLayout(self.layout)

    def init_buttons(self):
        # Code for the buttons
        self.button_widget = QWidget()
        self.button_layout = QHBoxLayout()
        self.button_save = QPushButton("Save solutions to png")
        self.button_save.clicked.connect(self.c.save_to_png)
        self.button_plan = QPushButton("Plan")
        self.button_plan.clicked.connect(self.c.plan)
        self.button_layout.addWidget(self.button_save)
        self.button_layout.addWidget(self.button_plan)
        self.button_widget.setLayout(self.button_layout)

    def init_board_settings(self):
        # Code for the board settings GUI
        self.board_widget = QWidget()
        self.label_width = QLabel("Width:")
        self.label_height = QLabel("Height:")
        self.le_width = qustom_line_edit()
        self.le_height = qustom_line_edit()

        self.board_layout = QGridLayout()
        self.board_layout.addWidget(self.label_width, 0, 0)
        self.board_layout.addWidget(self.le_width, 0, 1)
        self.board_layout.addWidget(self.label_height, 1, 0)
        self.board_layout.addWidget(self.le_height, 1, 1)

        self.board_widget.setLayout(self.board_layout)

    def get_input(self):
        width = int(self.le_width.text())
        height = int(self.le_height.text())
        return width, height

    def check_input(self):
        les = [self.le_width, self.le_height]
        for le in les:
            try:
                num = int(le.text())
            except ValueError:
                le.activate_wrong_input()
                return False
            if num <= 0:
                le.activate_wrong_input()
            le.deactivate_wrong_input()
        return True


class shape_widget(QWidget):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.table = qustom_table()
        self.table.setRowCount(2)
        self.table.setColumnCount(3)

        header = ["name", "Width", "Height"]
        self.table.setHorizontalHeaderLabels(header)
        self.table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.table.resizeColumnsToContents()

        self.table.cellChanged.connect(self.c.new_shape_row)

        self.buttons = QWidget()
        self.buttons_layout = QHBoxLayout()
        self.button_remove = QPushButton("Delete selected")
        self.button_remove.clicked.connect(self.c.remove_row)
        self.button_save = QPushButton("Save shapes")
        self.button_save.clicked.connect(self.c.save_shapes)
        self.button_load = QPushButton("Load shapes")
        self.button_load.clicked.connect(self.c.load_shapes)

        self.buttons_layout.addWidget(self.button_remove)
        self.buttons_layout.addWidget(self.button_save)
        self.buttons_layout.addWidget(self.button_load)
        self.buttons.setLayout(self.buttons_layout)

        self.layout.addWidget(self.table)
        self.layout.addWidget(self.buttons)
        self.setLayout(self.layout)

    def update_table(self):
        self.table.resizeColumnsToContents()

    def new_row(self):
        self.table.setRowCount(self.table.rowCount()+1)


class tab_solutions_widget(QTabWidget):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.tabs = QTabWidget()
        self.tab1 = empty_tab(self.c)

        self.tabs.addTab(self.tab1, "Solution")

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def show_solution(self, solution, shapes):
        print(solution)
        self.tablist = []
        self.tabs.setParent(None)
        self.tabs = QTabWidget()
        for index, board in enumerate(solution):
            tab = tab_drawing_widget(self.c)
            tab.draw_board(solution[board], shapes)
            self.tabs.addTab(tab, "Board {}".format(index))

        self.layout.addWidget(self.tabs)


class tab_drawing_widget(QTabWidget):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.rect_to_draw = []
        self.text_to_draw = []
        self.initUI()

    def initUI(self):
        self.painter = QPainter(self)
        pass

    def paintEvent(self, event):
        MARGIN = 50
        self.painter.begin(self)
        canvas = event.rect()
        scale_x = (canvas.width() - MARGIN)/self.board_width
        scale_y = (canvas.height() - MARGIN)/self.board_height
        scale = min(scale_x, scale_y)
        print(scale)

        self.drawboard_width = self.board_width * scale
        self.drawboard_height = self.board_height * scale

        self.offset_width = (canvas.width() - self.drawboard_width)/2
        self.offset_height = (canvas.height() - self.drawboard_height)/2
        for rect in self.rect_to_draw:
            self.painter.setBrush(rect[1])
            x1, y1, x2, y2 = rect[0]
            width = round((x2-x1) * scale)
            height = round((y2-y1) * scale)
            x1 = round(x1 * scale + self.offset_width)
            y1 = round(y1 * scale + self.offset_height)
            self.painter.drawRect(x1, y1, width, height)

        self.painter.setPen(QColor(0, 0, 0))
        self.painter.setFont(QFont('Helvetica', 10))
        for text in self.text_to_draw:
            x, y = text[0]
            x = round(x * scale + self.offset_width)
            y = round(y * scale + self.offset_height)
            self.painter.drawText(x, y, text[1])
        self.painter.end()

    def draw_board(self, board, shapes):
        print("Tab")
        print(board)
        for rect_list in board:
            if len(rect_list) == 2:  # Board
                color = QColor(255, 255, 255)
                self.rect_to_draw.append(([0, 0, rect_list[0], rect_list[1]],
                                         color))

                # Needed for position and scale
                self.board_width = rect_list[0]
                self.board_height = rect_list[1]
                self.biggest_edge = max(self.board_width, self.board_height)

                coor_t1 = (0, self.board_height//2)
                coor_t2 = (self.board_width//2, 0)
                self.text_to_draw.append((coor_t1, str(self.board_height)))
                self.text_to_draw.append((coor_t2, str(self.board_width)))

            elif len(rect_list) == 3:  # Shape
                color = QColor(179, 204, 204)
                tl_corner = rect_list[1]
                br_corner = rect_list[2]
                self.rect_to_draw.append(([tl_corner[0], tl_corner[1],
                                         br_corner[0], br_corner[1]],
                                         color))

                # Add the dimensions to the text to draw.
                width = br_corner[0] - tl_corner[0]
                height = br_corner[1] - tl_corner[1]
                coor_t1 = (br_corner[0], tl_corner[1] + height//2)
                coor_t2 = (tl_corner[0] + width//2, br_corner[1])
                self.text_to_draw.append((coor_t1, str(height)))
                self.text_to_draw.append((coor_t2, str(width)))

                # Add the name of the shapes to the text to draw.
                name = list(shapes.keys())[rect_list[0]]
                coor_t3 = (coor_t2[0], coor_t1[1])
                self.text_to_draw.append((coor_t3, str(name)))

            else:
                raise ValueError


class empty_tab(QTabWidget):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.label = qustom_title("Solution will show here.")
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)


class qustom_title(QLabel):
    """
    Een qustom custom_titlelabel.
    """
    def __init__(self, tekst):
        super().__init__(tekst)
        my_font = QFont()
        my_font.setBold(True)
        self.setFont(my_font)


class qustom_line_edit(QLineEdit):
    """
    """
    def __init__(self, value=None):
        super().__init__(value)
        self.alarm = False

    def activate_wrong_input(self):
        if not self.alarm:
            pal = QPalette()
            pal.setColor(self.backgroundRole(), Qt.red)
            self.setPalette(pal)
            self.alarm = True
        return

    def deactivate_wrong_input(self):
        if self.alarm:
            pal = QPalette()
            pal.setColor(self.backgroundRole(), Qt.white)
            self.setPalette(pal)
            self.alarm = False
        return


class qustom_table(QTableWidget):
    def __init__(self, value=None):
        super().__init__(value)
        self.row_alarms = []
        self.col_alarms = []
        self.brush_white = QBrush(Qt.white)
        self.brush_red = QBrush(Qt.red)

    def activate_wrong_row(self, row):
        if row in self.row_alarms:
            return

        for i in range(self.columnCount()):
            if self.item(row, i) is None:
                continue
            self.item(row, i).setBackground(self.brush_red)
        self.row_alarms.append(row)

    def activate_wrong_col(self, col):
        if col in self.col_alarms:
            return

        for i in range(self.rowCount()):
            if self.item(i, col) is None:
                continue
            self.item(i, col).setBackground(self.brush_red)
        self.col_alarms.append(col)

    def deactivate_wrong_row(self, row):
        if row not in self.row_alarms:
            return

        for i in range(self.rowCount()):
            if self.item(row, i) is None:
                continue
            self.item(row, i).setBackground(self.brush_white)
        self.row_alarms.remove(row)

    def deactivate_wrong_col(self, col):
        if col not in self.col_alarms:
            return

        for i in range(self.rowCount()):
            if self.item(i, col) is None:
                continue
            self.item(i, col).setBackground(self.brush_white)
        self.col_alarms.remove(col)


class loading_icon(QLabel):
    def __init__(self):
        super().__init__()
        self.setPixmap(QPixmap('Icons/hourglass.png'))
        self.setHidden(True)


class seperator(QFrame):
    """
    Custom klasse dat een horizontale of verticale lijn toont, als tussenschot.
    """
    def __init__(self, direction):
        super().__init__()
        if direction == "V":
            self.setFrameShape(QFrame.VLine)
        else:
            self.setFrameShape(QFrame.HLine)
        self.setLineWidth(1)


class read_only_item(QTableWidgetItem):
    """
    Custom tableitem dat read-only is.
    """
    def __init__(self, name):
        super().__init__(name)
        self.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)


def main():
    c = controller()


if __name__ == "__main__":
    main()


