# QCutter

QCutter is a simple sheet cutting software for the planning of cutting rectangular shapes out of rectangular boards.
It's perfect for DIY projects, like building furniture.

It's easy to use and easy to interpet. It's also 100% free and open source. The software is licensed under GPL 3.

For a list of all the features currently supported, see [Features](#features).
For a list of features to be implemented in the future, see [Future Features](#future).

If you would like to request a feature, feel free to add an [issue](https://gitlab.com/Salt_Factory/qcutter/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).




## <a name="features"></a> Features
* Cutting shapes out of a minimum amount of boards.
* Loading in CSV files containing shapes, for instance straight from Excel or LibreOffice.
* Saving the shapes to a CSV file.
* Saving the solution to png images.


## Installation

### Dependencies

* [PyQt5](https://www.riverbankcomputing.com/software/pyqt/download5)
* [PIL](https://python-pillow.org/)

When using pip:

```
pip3 install PyQt5 Pillow
```

### Windows

After installing the dependencies, download or clone the git repo and run **qcutter.py**.
### Linux
Installation is done by cloning the repo and running **qcutter.py**, as shown below.

```
git clone https://gitlab.com/Salt_Factory/qcutter QCutter
cd QCutter
python3 qcutter.py
```


## How to use

1. Add the dimensions of the board you would like to use.
2. For each shape, add a name, width and height. If you want to, you can save them to a CSV file by pressing **Save shapes** If you have a premade list of shapes in CSV format, you can load those in using **Load Shapes** (it needs to be named shapes.csv).
3. Click **Plan** to run the software.
4. View the solution in the right pane, and shuffle through multiple boards using the tabs.
5. Save the solution to png files by pressing **Save solutions to png**.
6. The images are saved in the folder where **qcutter.py** was run.

An example CSV file is included in the repo.


![example](https://thumbs.gfycat.com/ClassicQuaintIberianmidwifetoad-mobile.mp4)



## <a name="future"></a> Future Features
* Allowing for multiple boardsizes.
* Prebuilt Windows builds.



If there's anything else you'd like, don't hesitate to tell me.
